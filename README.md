# cm-cleaner

## Summary
This script provides cleans out a lot of cruft that I do not want from LineageOS.
* **Warning**: You might want some of these, and in fact the removal of some items here can leave your device in a broken state if you don't have a replacement installed BEFORE running this.

See `META-INF/com/google/android/updater-script` for a list of applications that this moves.


## Usage:
Zip the contents and flash in recovery after flashing LineageOS. This script has been tested on LineageOS 15.1, so running it on any other version may not work as intended.
